package com.reactive.apicross.persons.model.api.update.patch.personupdaterequest;

import com.reactive.apicross.persons.util.constants.Constants;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.Valid;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 * Bean <br>
 * <b>Class</b>: Natural<br>
 * <b>Copyright</b>: &copy; 2019 Mr. Peter.<br/>
 * <b>Company</b>: Mr.<br/>
 *
 * @author Peter Koe Vasquez Sotelo <br/>
 * <u>Service Provider</u>: Mr. <br/>
 * <u>Developed by</u>: <br/>
 * <ul>
 * <li>Peter Vasquez</li>
 * </ul>
 * <u>Changes</u>:<br/>
 * <ul>
 * <li>Oct 04, 2019 Creaci&oacute;n de Clase.</li>
 * </ul>
 * @version 1.0
 */
@Getter
@Setter
@NoArgsConstructor
public class Natural {

  @ApiModelProperty(
          position = 1,
          value = "Apellido Paterno del cliente",
          example = "VASQUEZ")
  @Size(max = 25)
  @Valid
  @Pattern(regexp = Constants.REGEXP_NAMES)
  private String fatherLastName;

  @ApiModelProperty(
          position = 2,
          value = "Apellido Materno del cliente",
          example = "SOTELO")
  @Size(max = 25)
  @Valid
  @Pattern(regexp = Constants.REGEXP_NAMES)
  private String motherLastName;

  @ApiModelProperty(
          position = 3,
          value = "Nombres del cliente",
          example = "PETER")
  @Size(max = 25)
  @Valid
  @Pattern(regexp = Constants.REGEXP_NAMES)
  private String names;

  @ApiModelProperty(
          position = 4,
          value = "Fecha de nacimiento",
          example = "1990-12-13")
  @Pattern(regexp = Constants.PATTERN_SIMPLE_DATE)
  private String birthDate;

}
