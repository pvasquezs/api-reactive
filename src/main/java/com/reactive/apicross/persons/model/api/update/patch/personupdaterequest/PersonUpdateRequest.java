package com.reactive.apicross.persons.model.api.update.patch.personupdaterequest;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Clase que representa un modelo de presentacion de datos que será expuesta por la api<br>
 * <b>Class</b>: PersonUpdateRequest<br>
 * <b>Copyright</b>: &copy; 2019 Mr. Peter.<br/>
 * <b>Company</b>: Mr.<br/>
 *
 * @author Peter Koe Vasquez Sotelo <br/>
 * <u>Service Provider</u>: Mr. <br/>
 * <u>Developed by</u>: <br/>
 * <ul>
 * <li>Peter Vasquez</li>
 * </ul>
 * <u>Changes</u>:<br/>
 * <ul>
 * <li>Oct 04, 2019 Creaci&oacute;n de Clase.</li>
 * </ul>
 * @version 1.0
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
@NoArgsConstructor
public class PersonUpdateRequest {

  @ApiModelProperty(
      name = "natural",
      value = "natural data",
      position = 1)
  @NotNull
  @Valid
  private Natural natural;
}
