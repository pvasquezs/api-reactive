package com.reactive.apicross.persons.model.api.create;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

/**
 * <br>
 * Clase POJO que encapsula los datos del Response
 * <b>Class</b>: PersonCreateResponse.java<br>
 * <b>Copyright</b>: &copy; 2019 Mr. Peter.<br/>
 * <b>Company</b>: Mr.<br/>
 *
 * @author Peter Koe Vasquez Sotelo <br/>
 * <u>Service Provider</u>: Mr. <br/>
 * <u>Developed by</u>: <br/>
 * <ul>
 * <li>Peter Vasquez</li>
 * </ul>
 * <u>Changes</u>:<br/>
 * <ul>
 * <li>Oct 10, 2019 Creaci&oacute;n de Clase.</li>
 * </ul>
 * @version 1.0
 */
@SuppressFBWarnings(
    value = "NM_CONFUSING",
    justification = "Attributes cic have similar names with the Thirdparty.")
@Getter
@Setter
public class PersonCreateResponse implements Serializable {

  private static final long serialVersionUID = 3063217690829817475L;

  @ApiModelProperty(
      position = 1,
      name = "personId",
      value = "IDC del cliente.",
      example = "758172751",
      required = false)
  private String personId;

  @ApiModelProperty(
      position = 2,
      name = "dni",
      value = "DNI del cliente.",
      example = "72795533",
      required = false)
  private String dni;

  @ApiModelProperty(
      position = 3,
      name = "operationResults",
      value = "OperationResults.",
      required = false)
  private List<OperationResultsResponse> operationResults;
}
