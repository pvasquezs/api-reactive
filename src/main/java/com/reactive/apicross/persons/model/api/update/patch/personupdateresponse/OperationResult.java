package com.reactive.apicross.persons.model.api.update.patch.personupdateresponse;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

/**
 * Class Bean<br>
 * <b>Class</b>: OperationResult <br>
 * <b>Copyright</b>: &copy; 2019 Mr. Peter.<br/>
 * <b>Company</b>: Mr.<br/>
 *
 * @author Peter Koe Vasquez Sotelo <br/>
 * <u>Service Provider</u>: Mr. <br/>
 * <u>Developed by</u>: <br/>
 * <ul>
 * <li>Peter Vasquez</li>
 * </ul>
 * <u>Changes</u>:<br/>
 * <ul>
 * <li>Oct 04, 2019 Creaci&oacute;n de Clase.</li>
 * </ul>
 * @version 1.0
 */
@Getter
@Setter
@ApiModel(value = "OperationResult", description = "Datos del error de sistema.")
public class OperationResult implements Serializable {

  private static final long serialVersionUID = 1L;

  @ApiModelProperty(
      name = "relatedData",
      value = "Datos relacionados",
      example = "DATOS SENSIBLES",
      required = true,
      position = 1)
  private String relatedData;

  @ApiModelProperty(
      name = "status",
      value = "status data",
      required = true,
      position = 2)
  private Status status;

  @ApiModelProperty(
      name = "exceptionDetails",
      value = "exceptionDetails data",
      position = 3)
  private List<ExceptionDetails> exceptionDetails;
}
