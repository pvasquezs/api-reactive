package com.reactive.apicross.persons.model.api.create;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * <br>
 * Clase POJO que encapsula los detalles de los status <b>Class</b>: Status.java<br>
 * <b>Copyright</b>: &copy; 2019 Mr. Peter.<br/>
 * <b>Company</b>: Mr.<br/>
 *
 * @author Peter Koe Vasquez Sotelo <br/>
 * <u>Service Provider</u>: Mr. <br/>
 * <u>Developed by</u>: <br/>
 * <ul>
 * <li>Peter Vasquez</li>
 * </ul>
 * <u>Changes</u>:<br/>
 * <ul>
 * <li>Oct 14, 2019 Creaci&oacute;n de Clase.</li>
 * </ul>
 * @version 1.0
 */
@Getter
@Setter
public class StatusResponse implements Serializable {

  private static final long serialVersionUID = -7672191309100451728L;

  @ApiModelProperty(
          position = 1,
          value = "code",
          example = "MR0000")
  private String code;

  @ApiModelProperty(
          position = 2,
          value = "description",
          example = "PROCESO CONFORME")
  private String description;
}
