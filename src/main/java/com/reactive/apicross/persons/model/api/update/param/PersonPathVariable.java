package com.reactive.apicross.persons.model.api.update.param;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiParam;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Objeto que recibe el personId.
 * <b>Class</b>: PersonsController<br>
 * <b>Copyright</b>: &copy; 2019 Mr. Peter.<br/>
 * <b>Company</b>: Mr.<br/>
 *
 * @author Peter Koe Vasquez Sotelo <br/>
 * <u>Service Provider</u>: Mr. <br/>
 * <u>Developed by</u>: <br/>
 * <ul>
 * <li>Peter Vasquez</li>
 * </ul>
 * <u>Changes</u>:<br/>
 * <ul>
 * <li>Oct 04, 2019 Creaci&oacute;n de Clase.</li>
 * </ul>
 * @version 1.0
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class PersonPathVariable implements CharSequence {

  @ApiParam(
      name = "personId",
      example = "202030301000",
      hidden = true,
      required = true,
      type = "String",
      value = "Identificador del Cliente")
  @Size(min = 5, max = 15)
  @NotNull(message = "No debe ser nulo.")
  @NotEmpty(message = "No debe ser vacio")
  @Pattern(regexp = "([a-zA-Z\\d\\-]{1,11}[ABCDEFLM]([\\d]{3})|[a-zA-Z\\d\\-]{1,11}[1234567](000))")
  private String personId;

  @Override
  public int length() {
    return this.personId.length();
  }

  @Override
  public char charAt(int index) {
    return this.getPersonId().charAt(index);
  }

  @Override
  public CharSequence subSequence(int start, int end) {
    return getPersonId().subSequence(start, end);
  }
}
