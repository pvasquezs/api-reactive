package com.reactive.apicross.persons.model.api.create;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.reactive.apicross.persons.util.constants.Constants;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.Valid;
import javax.validation.constraints.Pattern;
import java.io.Serializable;

/**
 * <br>
 * <b>Class</b>: NaturalCreation.<br>
 * <b>Copyright</b>: &copy; 2019 Mr. Peter.<br/>
 * <b>Company</b>: Mr.<br/>
 *
 * @author Peter Koe Vasquez Sotelo <br/>
 * <u>Service Provider</u>: Mr. <br/>
 * <u>Developed by</u>: <br/>
 * <ul>
 * <li>Peter Vasquez</li>
 * </ul>
 * <u>Changes</u>:<br/>
 * <ul>
 * <li>Oct 09, 2019 Creaci&oacute;n de Clase.</li>
 * </ul>
 * @version 1.0
 */
@Getter
@Setter
@ToString
public class NaturalCreation implements Serializable {

  private static final long serialVersionUID = -4155451781606618448L;

  @Valid
  @JsonProperty("referenceCode")
  @ApiModelProperty(
      position = 1,
      value = "Codigo del cliente en el Karaoke")
  private String referenceCode;

  @Valid
  @JsonProperty("fatherLastName")
  @Pattern(regexp = Constants.REGEXP_NAMES)
  @ApiModelProperty(
      position = 2,
      value = "Apellido Paterno del cliente",
      example = "VASQUEZ")
  private String fatherLastName;

  @Valid
  @JsonProperty("motherLastName")
  @Pattern(regexp = Constants.REGEXP_NAMES)
  @ApiModelProperty(
      position = 3,
      value = "Apellido Materno del cliente",
      example = "SOTELO")
  private String motherLastName;

  @Valid
  @JsonProperty("names")
  @Pattern(regexp = Constants.REGEXP_NAMES)
  @ApiModelProperty(
      position = 4,
      value = "Nombres del cliente",
      example = "PETER")
  private String names;

  @Valid
  @JsonProperty("birthDate")
  @Pattern(regexp = Constants.PATTERN_SIMPLE_DATE)
  @ApiModelProperty(
      position = 5,
      value = "Fecha de nacimiento",
      example = "1990-12-13")
  private String birthDate;

}
