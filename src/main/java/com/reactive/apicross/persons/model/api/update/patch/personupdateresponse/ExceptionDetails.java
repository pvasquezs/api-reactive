package com.reactive.apicross.persons.model.api.update.patch.personupdateresponse;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * Class Bean<br>
 * <b>Class</b>: ExceptionDetails <br>
 * <b>Copyright</b>: &copy; 2019 Mr. Peter.<br/>
 * <b>Company</b>: Mr.<br/>
 *
 * @author Peter Koe Vasquez Sotelo <br/>
 * <u>Service Provider</u>: Mr. <br/>
 * <u>Developed by</u>: <br/>
 * <ul>
 * <li>Peter Vasquez</li>
 * </ul>
 * <u>Changes</u>:<br/>
 * <ul>
 * <li>Oct 04, 2019 Creaci&oacute;n de Clase.</li>
 * </ul>
 * @version 1.0
 */
@Getter
@Setter
@ApiModel(value = "ExceptionDetails", description = "Datos del error de sistema.")
public class ExceptionDetails implements Serializable {
  private static final long serialVersionUID = 1L;

  @ApiModelProperty(
      name = "code",
      value = "Codigo del error",
      example = "MB0001",
      position = 1)
  private String code;

  @ApiModelProperty(
      name = "description",
      value = "descripcion del error",
      example = "Error al ejecutar el servicio",
      position = 2)
  private String description;

  @ApiModelProperty(
      name = "component",
      value = "nombre del componente",
      example = "CltDatosLabUpdV1",
      position = 3)
  private String component;
}
