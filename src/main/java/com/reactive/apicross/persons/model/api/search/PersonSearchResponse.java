package com.reactive.apicross.persons.model.api.search;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * Clase que representa un modelo de presentacion de datos
 * que será expuesta por la api<br/>
 * <b>Class</b>: PersonSearchResponse<br/>
 * <b>Copyright</b>: &copy; 2019 Mr. Peter.<br/>
 * <b>Company</b>: Mr.<br/>
 *
 * @author Peter Koe Vasquez Sotelo <br/>
 * <u>Service Provider</u>: Mr. <br/>
 * <u>Developed by</u>: <br/>
 * <ul>
 * <li>Peter Vasquez</li>
 * </ul>
 * <u>Changes</u>:<br/>
 * <ul>
 * <li>Oct 04, 2019 Creaci&oacute;n de Clase.</li>
 * </ul>
 * @version 1.0
 */
@Getter
@Setter
@JsonPropertyOrder({"personId", "names"})
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PersonSearchResponse {

  @ApiModelProperty(
          name = "personId",
          value = "Identificador del cliente",
          example = "074035341",
          dataType = "string",
          position = 1)
  private String personId;

  @ApiModelProperty(
          name = "names",
          value = "Nombres",
          example = "FREDY",
          dataType = "string",
          position = 2)
  private String names;

}
