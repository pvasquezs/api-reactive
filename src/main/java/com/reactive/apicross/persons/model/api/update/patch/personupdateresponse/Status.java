package com.reactive.apicross.persons.model.api.update.patch.personupdateresponse;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * Bean <b>Class</b>: StatusResponse<br>
 * <b>Copyright</b>: &copy; 2019 Mr. Peter.<br/>
 * <b>Company</b>: Mr.<br/>
 *
 * @author Peter Koe Vasquez Sotelo <br/>
 * <u>Service Provider</u>: Mr. <br/>
 * <u>Developed by</u>: <br/>
 * <ul>
 * <li>Peter Vasquez</li>
 * </ul>
 * <u>Changes</u>:<br/>
 * <ul>
 * <li>Oct 04, 2019 Creaci&oacute;n de Clase.</li>
 * </ul>
 * @version 1.0
 */
@Getter
@Setter
@ApiModel(value = "Status", description = "Datos del error de sistema.")
public class Status implements Serializable {

  private static final long serialVersionUID = 1L;

  @ApiModelProperty(
      name = "code",
      value = "Codigo de Operacion",
      example = "TL0001",
      required = true,
      position = 1)
  private String code;

  @ApiModelProperty(
      name = "description",
      value = "Descripcion de la operacion",
      example = "ERROR FUNCIONAL EN ACTUALIZACION",
      required = true,
      position = 2)
  private String description;
}
