package com.reactive.apicross.persons.model.api.create;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;

/**
 * <br>
 * Clase POJO que encapsula los datos del request
 * <b>Class</b>: PersonsCreateRequest.java<br>
 * <b>Copyright</b>: &copy; 2019 Mr. Peter.<br/>
 * <b>Company</b>: Mr.<br/>
 *
 * @author Peter Koe Vasquez Sotelo <br/>
 * <u>Service Provider</u>: Mr. <br/>
 * <u>Developed by</u>: <br/>
 * <ul>
 * <li>Peter Vasquez</li>
 * </ul>
 * <u>Changes</u>:<br/>
 * <ul>
 * <li>Oct 09, 2019 Creaci&oacute;n de Clase.</li>
 * </ul>
 * @version 1.0
 */
@Getter
@Setter
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(value = Include.NON_NULL)
public class PersonCreateRequest implements Serializable {

  private static final long serialVersionUID = -8163763697521075700L;

  @Valid
  @NotNull
  @NotEmpty
  @JsonProperty("personId")
  @Size(min = 5, max = 15, message = "El campo personId debe tener entre 5 y 15 caracteres")
  @Pattern(regexp = "([a-zA-Z\\d\\-]{1,11}[ABCDEFLM]([\\d]{3})|[a-zA-Z\\d\\-]{1,11}[1234567](000))")
  @ApiModelProperty(
      position = 1,
      value = "Identificador de la persona",
      example = "449037371000",
      required = true)
  private String personId;

  @Valid
  @JsonProperty("natural")
  @ApiModelProperty(position = 2, value = "Persona Natural")
  private NaturalCreation natural;

}
