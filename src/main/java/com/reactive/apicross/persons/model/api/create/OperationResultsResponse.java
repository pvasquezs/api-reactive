package com.reactive.apicross.persons.model.api.create;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import java.io.Serializable;
import java.util.List;

/**
 * <br>
 * Clase POJO que encapsula los detalles de los Resultados de las Operaciones
 * <b>Class</b>: OperationResultsResponse.java<br>
 * <b>Copyright</b>: &copy; 2019 Mr. Peter.<br/>
 * <b>Company</b>: Mr.<br/>
 *
 * @author Peter Koe Vasquez Sotelo <br/>
 * <u>Service Provider</u>: Mr. <br/>
 * <u>Developed by</u>: <br/>
 * <ul>
 * <li>Peter Vasquez</li>
 * </ul>
 * <u>Changes</u>:<br/>
 * <ul>
 * <li>Oct 10, 2019 Creaci&oacute;n de Clase.</li>
 * </ul>
 * @version 1.0
 */
@Getter
@Setter
public class OperationResultsResponse implements Serializable {

  private static final long serialVersionUID = -7395081361343388930L;

  @ApiModelProperty(
          position = 1,
          value = "relatedData",
          example = "DATOS BASICOS DE PERSONA")
  private String relatedData;

  @ApiModelProperty(
          position = 2,
          value = "status")
  private StatusResponse status;

  @ApiModelProperty(
          position = 3,
          value = "exceptionDetails",
          required = true)
  private List<ExceptionDetailResponse> exceptionDetails;
}
