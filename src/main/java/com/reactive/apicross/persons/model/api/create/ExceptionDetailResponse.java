package com.reactive.apicross.persons.model.api.create;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * <br>
 * Clase POJO que encapsula los detalles de las excepciones
 * <b>Class</b>: ExceptionDetailResponse.java<br>
 * <b>Copyright</b>: &copy; 2019 Mr. Peter.<br/>
 * <b>Company</b>: Mr.<br/>
 *
 * @author Peter Koe Vasquez Sotelo <br/>
 * <u>Service Provider</u>: Mr. <br/>
 * <u>Developed by</u>: <br/>
 * <ul>
 * <li>Peter Vasquez</li>
 * </ul>
 * <u>Changes</u>:<br/>
 * <ul>
 * <li>Oct 10, 2019 Creaci&oacute;n de Clase.</li>
 * </ul>
 * @version 1.0
 */
@Getter
@Setter
public class ExceptionDetailResponse implements Serializable {

  private static final long serialVersionUID = 3391401502341917783L;

  @ApiModelProperty(
          position = 1,
          value = "code",
          example = "MB0000")
  private String code;

  @ApiModelProperty(
      position = 2,
      value = "description",
      example = "Direccion Online IdSec=02 NO registrada")
  private String description;

  @ApiModelProperty(
          position = 3,
          value = "component",
          example = "PRTW")
  private String component;
}
