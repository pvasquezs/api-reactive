package com.reactive.apicross.persons.business.create;

import com.reactive.apicross.persons.model.api.create.PersonCreateRequest;
import com.reactive.apicross.persons.model.api.create.PersonCreateResponse;
import io.reactivex.Single;
import javax.xml.bind.JAXBException;

/**
 * <br/>
 * Clase Interfaz del Servicio para la logica de negocio que consumira la clase REST
 * <b>Class</b>: PersonController<br/>
 * <b>Copyright</b>: &copy; 2019 Mr. Peter<br/>
 * <b>Company</b>: Mr.<br/>
 *
 * @author Peter Koe Vasquez Sotelo <br/>
 * <u>Service Provider</u>: Mr. <br/>
 * <u>Developed by</u>: <br/>
 * <ul>
 * <li>Peter Vasquez</li>
 * </ul>
 * <u>Changes</u>:<br/>
 * <ul>
 * <li>Oct 04, 2019 Creaci&oacute;n de Clase.</li>
 * </ul>
 * @version 1.0
 */

public interface PersonCreateService {
  Single<PersonCreateResponse> createPerson(PersonCreateRequest request)
      throws JAXBException;
}
