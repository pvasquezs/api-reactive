package com.reactive.apicross.persons.business.search.impl;

import com.reactive.apicross.persons.business.search.PersonSearchService;
import com.reactive.apicross.persons.model.api.search.PersonSearchResponse;
import io.reactivex.Observable;
import io.reactivex.Single;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * <br/> Clase service que contiene los metodos necesarios para tramitar la data
 * y logica de negocio que consumira la clase REST PersonController<br/>
 * <b>Class</b>: PersonSearchServiceImpl<br/>
 * <b>Copyright</b>: &copy; 2019 Mr. Peter<br/>
 * <b>Company</b>: Mr.<br/>
 *
 * @author Peter Koe Vasquez Sotelo <br/>
 * <u>Service Provider</u>: Mr. <br/>
 * <u>Developed by</u>: <br/>
 * <ul>
 * <li>Peter Vasquez</li>
 * </ul>
 * <u>Changes</u>:<br/>
 * <ul>
 * <li>Oct 07, 2019 Creaci&oacute;n de Clase.</li>
 * </ul>
 * @version 1.0
 */
@Service
@Slf4j
public class PersonSearchServiceImpl implements PersonSearchService {

  @Override
  public Observable<PersonSearchResponse> getPerson() {
    log.info("Starting PersonSearchServiceImpl.getPerson");
    return Observable.empty();
  }

  @Override
  public Single<PersonSearchResponse> searchPersonByPersonId(String personId) {
    log.info("Starting PersonSearchServiceImpl.searchPersonByPersonId");
    return null;
  }

}
