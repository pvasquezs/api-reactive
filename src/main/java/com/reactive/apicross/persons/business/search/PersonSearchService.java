package com.reactive.apicross.persons.business.search;

import com.reactive.apicross.persons.model.api.search.PersonSearchResponse;
import io.reactivex.Observable;
import io.reactivex.Single;

/**
 * <br/> Clase Interfaz del Servicio para la logica de negocio que consumira
 * la clase REST PersonController<br/>
 * <b>Class</b>: PersonSearchService<br/>
 * <b>Copyright</b>: &copy; 2019 Mr. Peter<br/>
 * <b>Company</b>: Mr.<br/>
 *
 * @author Peter Koe Vasquez Sotelo <br/>
 * <u>Service Provider</u>: Mr. <br/>
 * <u>Developed by</u>: <br/>
 * <ul>
 * <li>Peter Vasquez</li>
 * </ul>
 * <u>Changes</u>:<br/>
 * <ul>
 * <li>Oct 04, 2019 Creaci&oacute;n de Clase.</li>
 * </ul>
 * @version 1.0
 */
public interface PersonSearchService {

  public Observable<PersonSearchResponse> getPerson();

  public Single<PersonSearchResponse> searchPersonByPersonId(String personId);

}
