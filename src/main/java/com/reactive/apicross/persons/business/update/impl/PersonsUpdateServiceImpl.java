package com.reactive.apicross.persons.business.update.impl;

import com.reactive.apicross.persons.business.update.PersonsUpdateService;
import com.reactive.apicross.persons.model.api.update.patch.personupdaterequest.PersonUpdateRequest;
import com.reactive.apicross.persons.model.api.update.patch.personupdateresponse.PersonUpdateResponse;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import io.reactivex.Single;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * <br/> Clase service que contiene los metodos necesarios para tramitar la data
 * y logica de negocio que consumira la clase REST PersonController<br/>
 * <b>Class</b>: PersonsUpdateServiceImpl<br/>
 * <b>Copyright</b>: &copy; 2019 Mr. Peter<br/>
 * <b>Company</b>: Mr.<br/>
 *
 * @author Peter Koe Vasquez Sotelo <br/>
 * <u>Service Provider</u>: Mr. <br/>
 * <u>Developed by</u>: <br/>
 * <ul>
 * <li>Peter Vasquez</li>
 * </ul>
 * <u>Changes</u>:<br/>
 * <ul>
 * <li>Oct 07, 2019 Creaci&oacute;n de Clase.</li>
 * </ul>
 * @version 1.0
 */
@SuppressFBWarnings(
        value = "PRMC_POSSIBLY_REDUNDANT_METHOD_CALLS",
        justification = "There isn't redundant calls here."
)

@Service
@Slf4j
public class PersonsUpdateServiceImpl implements PersonsUpdateService {

  @Override
  public Single<PersonUpdateResponse> updatePerson(PersonUpdateRequest personUpdateRequest, String personId) {
    log.debug("Income to PersonsUpdateServiceImpl.updatePerson");
    return null;
  }
}
