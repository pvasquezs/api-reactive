package com.reactive.apicross.persons.business.create.impl;

import com.reactive.apicross.persons.business.create.PersonCreateService;
import com.reactive.apicross.persons.model.api.create.PersonCreateRequest;
import com.reactive.apicross.persons.model.api.create.PersonCreateResponse;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import io.reactivex.Single;
import lombok.AllArgsConstructor;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.xml.bind.JAXBException;

/**
 * <br/>
 * Clase que implementa los servicios para las funcionalidades de direcciones de clientes
 * <b>Class</b>: PersonCreateServiceImpl<br/>
 * <b>Copyright</b>: &copy; 2019 Mr. Peter<br/>
 * <b>Company</b>: Mr.<br/>
 *
 * @author Peter Koe Vasquez Sotelo <br/>
 * <u>Service Provider</u>: Mr. <br/>
 * <u>Developed by</u>: <br/>
 * <ul>
 * <li>Peter Vasquez</li>
 * </ul>
 * <u>Changes</u>:<br/>
 * <ul>
 * <li>Oct 07, 2019 Creaci&oacute;n de Clase.</li>
 * </ul>
 * @version 1.0
 */
@SuppressFBWarnings(
    value = "PRMC_POSSIBLY_REDUNDANT_METHOD_CALLS",
    justification = "Call to suscribeOn.io for each method for distinct brokers.")
@AllArgsConstructor
@Slf4j
@Service
@ToString
public class PersonCreateServiceImpl implements PersonCreateService {

  @Override
  public Single<PersonCreateResponse> createPerson(PersonCreateRequest request) throws JAXBException {
    log.debug("Entrando al consumo del servicio... ");
    return null;
    }
    
  }
