package com.reactive.apicross.persons.util.constants;

/**
 * Clase que representa Constantes<br>
 * <b>Class</b>: Constants<br>
 * <b>Copyright</b>: &copy; 2019 Mr. Peter<br/>
 * <b>Company</b>: Mr.<br/>
 *
 * @author Peter Koe Vasquez Sotelo <br/>
 * <u>Service Provider</u>: Mr. <br/>
 * <u>Developed by</u>: <br/>
 * <ul>
 * <li>Peter Vasquez</li>
 * </ul>
 * <u>Changes</u>:<br/>
 * <ul>
 * <li>Oct 07, 2019 Creaci&oacute;n de Clase.</li>
 * </ul>
 * @version 1.0
 */
public class Constants {

  private Constants() {
  }

  public static final String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";
  public static final String REGEXP_NAMES =
          "^((?!</)(?!;--%)(?!\\);)[^!\\[\\]^|¡¢£¤¥¦§¨©ª«¬®¯°±²³´µ¶·¹º»¼½¾¿Þß÷øþ@=\\\\])*$";
  public static final String PATTERN_SIMPLE_DATE =
          "^((19|20)\\d\\d)-(0?[1-9]|1[012])-(0?[1-9]|[12][0-9]|3[01])$";
  public static final String PATTERN_EMAIL =
          "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
  public static final String APP_DATE_FORMAT_RESPONSE = "yyyy-MM-dd";
  public static final String APP_DATE_FULL_FORMAT_RESPONSE = "yyyy-MM-dd'T'HH:mm:ss";
  public static final String REGEX_CUSTOMER_CODE = "CLIENT|DJ";

}
