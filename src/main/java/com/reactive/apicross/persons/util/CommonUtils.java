package com.reactive.apicross.persons.util;

import com.reactive.apicross.persons.util.constants.Constants;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Clase que brinda m&eacute;todos de soporte para tareas del proyecto<br>
 * <b>Class</b>: CommonUtils<br>
 * <b>Copyright</b>: &copy; 2019 Mr. Peter<br/>
 * <b>Company</b>: Mr.<br/>
 *
 * @author Peter Koe Vasquez Sotelo <br/>
 * <u>Service Provider</u>: Mr. <br/>
 * <u>Developed by</u>: <br/>
 * <ul>
 * <li>Peter Vasquez</li>
 * </ul>
 * <u>Changes</u>:<br/>
 * <ul>
 * <li>Oct 07, 2019 Creaci&oacute;n de Clase.</li>
 * </ul>
 * @version 1.0
 */
public class CommonUtils {

  private CommonUtils() {
    //util class
  }

  /**
   * Common Utils .
   */
  public static String dateToString(Date date) {
    DateFormat formatter = new SimpleDateFormat(Constants.DATE_FORMAT);
    return formatter.format(date);
  }
}
