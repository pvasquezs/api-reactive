package com.reactive.apicross;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApicrossApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApicrossApplication.class, args);
	}

}
