package com.reactive.apicross.exponse.web;

import com.reactive.apicross.persons.business.create.PersonCreateService;
import com.reactive.apicross.persons.business.search.PersonSearchService;
import com.reactive.apicross.persons.business.update.PersonsUpdateService;
import com.reactive.apicross.persons.model.api.create.PersonCreateRequest;
import com.reactive.apicross.persons.model.api.create.PersonCreateResponse;
import com.reactive.apicross.persons.model.api.search.PersonSearchResponse;
import com.reactive.apicross.persons.model.api.update.param.PersonPathVariable;
import com.reactive.apicross.persons.model.api.update.patch.personupdaterequest.PersonUpdateRequest;
import com.reactive.apicross.persons.model.api.update.patch.personupdateresponse.PersonUpdateResponse;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ServerWebExchange;

import javax.validation.constraints.NotNull;
import javax.xml.bind.JAXBException;

/**
 * Controlador principal que expone el servicio a trav&eacute;s de HTTP/Rest para
 * las operaciones del recurso Persons<br/>
 * <b>Class</b>: PersonController<br/>
 * <b>Copyright</b>: &copy; 2019 Mr. Peter<br/>
 * <b>Company</b>: Mr.<br/>
 *
 * @author Peter Koe Vasquez Sotelo <br/>
 * <u>Service Provider</u>: Mr. <br/>
 * <u>Developed by</u>: <br/>
 * <ul>
 * <li>Peter Vasquez</li>
 * </ul>
 * <u>Changes</u>:<br/>
 * <ul>
 * <li>Oct 04, 2019 Creaci&oacute;n de Clase.</li>
 * </ul>
 * @version 1.0
 */
@RefreshScope
@RestController
@Api(tags = "Persons")
@RequestMapping("/mr/karaoke-box/v1")
@Slf4j
public class PersonController {

  @Autowired
  private PersonSearchService personSearchService;

  @Autowired
  private PersonCreateService personCreateService;

  @Autowired
  private PersonsUpdateService personsUpdateService;

  /**
   * Metodo que obtiene el listado de clientes.
   *
   * @return {@link Observable}
   */
  @GetMapping(value = "/persons", produces = MediaType.APPLICATION_JSON_VALUE)
  @ApiOperation(
          value = "Consulta de clientes",
          produces = MediaType.APPLICATION_STREAM_JSON_VALUE,
          consumes = MediaType.APPLICATION_STREAM_JSON_VALUE,
          response = Class.class,
          responseContainer = "List",
          httpMethod = "GET",
          notes = "classpath:swagger/notes/person-search.md")
  @ApiResponses({
          @ApiResponse(code = 200, message = "Obtuvo el listado solicitado."),
          @ApiResponse(code = 404, message = "No se encontr&oacute; el recurso solicitado."),
          @ApiResponse(code = 500, message = "Error al realizar la busqueda de datos."),
          @ApiResponse(code = 503, message = "El servicio no se encuentra disponible.")})
  public Observable<PersonSearchResponse> getPersons() {
    return personSearchService.getPerson();
  }

  /**
   * Metodo que obtiene la informacion de un cliente.
   *
   * @return {@link Single}
   */
  @GetMapping(value = "/persons/{personId}")
  @ResponseStatus(HttpStatus.OK)
  @ApiOperation(
          value = "Obtiene el detalle de un cliente.",
          produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
          consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
          response = PersonSearchResponse.class,
          httpMethod = "GET",
          notes = "classpath:swagger/notes/person-search-by-id.md")
  @ApiResponses({
          @ApiResponse(code = 200, message = "Se obtuvo el detalle de la persona correctamente"),
          @ApiResponse(code = 400, message = "El cliente envi&oacute; datos incorrectos."),
          @ApiResponse(code = 404, message = "No se encontr&oacute; el recurso solicitado."),
          @ApiResponse(code = 500, message = "Error al obtener el detalle de la persona"),
          @ApiResponse(code = 503, message = "El servicio no se encuentra disponible.")})
  @SuppressFBWarnings(value = "SPRING_ENDPOINT", justification = "This method cannot be reachable remotely")
  public Single<PersonSearchResponse> searchPersonByPersonId(
          @ApiParam(name = "personId", value = "Identificador de la persona",
                  example = "423608871000", type = "string", required = true)
          @NotNull
          @PathVariable String personId
  ) {
    log.info("Starting PersonController.searchPersonByPersonId method");
    return personSearchService.searchPersonByPersonId(personId)
            .doFinally(() -> log.info("Finished PersonController.searchPersonByPersonId method"));
  }

  /**
   * Metodo asociado al endpoint creacion de clientes.
   *
   * @param request {@link PersonCreateRequest}
   * @return {@link PersonCreateResponse}
   */
  @PostMapping(value = "/persons", produces = {MediaType.APPLICATION_JSON_UTF8_VALUE,
          MediaType.APPLICATION_STREAM_JSON_VALUE})
  @ResponseStatus(HttpStatus.OK)
  @ApiOperation(
          value = "Registra los datos de un cliente",
          produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
          consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
          notes = "classpath:swagger/notes/person-register.md", httpMethod = "POST")
  @ApiResponses({
          @ApiResponse(code = 200, message = "La peticion se proceso correctamente."),
          @ApiResponse(code = 400, message = "El cliente envio datos incorrectos."),
          @ApiResponse(code = 500, message = "Error al intentar actualizar el recurso."),
          @ApiResponse(code = 501, message = "Funcionalidad no implementada."),
          @ApiResponse(code = 503, message = "El servicio no se encuentra disponible.")})
  public Single<PersonCreateResponse> createPerson(
          @RequestBody PersonCreateRequest request,
          ServerWebExchange serverWebExchange)
          throws JAXBException {
    log.debug("Starting PersonController.createPerson method");
    return personCreateService.createPerson(request)
            .map(person -> {
              if (person.getOperationResults() != null
                      && !person.getOperationResults().isEmpty()
                      && person.getOperationResults().get(0).getExceptionDetails() != null) {
                if (person.getDni() == null) { // No se creo

                } else { // Creado parcialmente
                  serverWebExchange.getResponse().setStatusCode(HttpStatus.PARTIAL_CONTENT);
                }
              }
              return person;
            });
  }

  /**
   * Controller updatePerson.
   *
   * @param personUpdateRequest {PersonUpdateRequest}
   * @param personId            {String}
   * @return PersonUpdateResponse
   */
  @ApiOperation(
          value = "Realiza la actualizacion de datos basicos de un cliente.",
          produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
          consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
          httpMethod = "PATCH",
          response = PersonUpdateResponse.class,
          notes = "classpath:/swagger/notes/person-update.md")
  @ApiResponses({
          @ApiResponse(code = 204, message = "Se actualizo los datos correctamente.", response = Completable.class),
          @ApiResponse(code = 206, message = "No se pudo actualizar correctamente",
                  response = PersonUpdateResponse.class),
          @ApiResponse(code = 500, message = "Error en la conexion.")
  })
  @PatchMapping(value = "/persons/{personId}", produces = {
          MediaType.APPLICATION_JSON_UTF8_VALUE, MediaType.APPLICATION_STREAM_JSON_VALUE})
  @ResponseStatus(HttpStatus.PARTIAL_CONTENT)
  public Single<PersonUpdateResponse> updatePerson(
          @RequestBody PersonUpdateRequest personUpdateRequest,
          @PathVariable(value = "personId") PersonPathVariable personId,
          ServerHttpResponse response) {
    return personsUpdateService
            .updatePerson(personUpdateRequest, personId.getPersonId())
            .map(
                    personUpdateResponse -> {
                      if (personUpdateResponse.getOperationResults() == null) {
                        response.setStatusCode(HttpStatus.NO_CONTENT);
                        return new PersonUpdateResponse();
                      } else {
                        response.setStatusCode(HttpStatus.PARTIAL_CONTENT);
                        return personUpdateResponse;
                      }
                    });
  }

}
