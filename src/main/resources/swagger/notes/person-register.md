### Acerca de la funcionalidad expuesta
La funcionalidad permite la creacion de un Cliente.

   •   Los campos referentes a nombres, apellidos y direcciones presentan restricciones de caracteres.

### URI de acceso a la API
| Metodo            | URI                                                    |
|-------------------|--------------------------------------------------------|
| POST              | /mr/karaoke-box/v1/persons |

### Precondiciones para el consumo de esta versión de la API
Para poder consumir el API, el canal deberá registrar su codigo en Mr.

### Headers Requeridos
| Header    | Ejemplo |
|-----------|---------|
| Request-ID | 550e8400-e29b-41d4-a716-446655440000 |
| request-date | 2017-06-01T17:15:20.509-0400 |
| app-code | KB |
| app-name | KBOX |
| caller-name | karaoke-box |

### Query Parameters validos en esta version de la API
No aplica.

### Usos validos de Query Parameters
No aplica.

### Variantes validas del Payload (Cuerpo del mesaje)
No Aplica.

### Caso 1. Registro de Cliente - Persona Natural (PN01)
```
{
	"personId": "443552341000",															
	"natural":{
		"referenceCode": "000000000",
		"fatherLastName": "VASQUEZ",
		"motherLastName": "SOTELO",                                                   	
		"names": "PETER",                                                           	
		"birthDate": "1990-03-31"
	}
}
```

### Lista de Valores usadas en esta version de la API
A continuación se listan aquellos campos que tienen mas de un valor posible.

| Atributo |
|----------|
| request.personId |
| request.natural.ReferenceCode |
| request.natural.fatherLastName |
| request.natural.motherLastName |
| request.natural.names |
| request.natural.birthDate |

### Codigos de error usados en esta version de la API
| Codigo | Descripcion |
|--------|-------------|
| MR0000 | El servicio se ejecutó de manera satisfactoria                           |
| MR9999  | Ocurrió un error inesperado. Por favor contactarse con Soporte Técnico  |
