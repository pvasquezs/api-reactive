### Acerca de la funcionalidad expuesta

Actualizacion de los datos del cliente

   • Los campos referentes a nombres y apellidos presentan restricciones de caracteres No validos.

### URI de acceso a la API
| Método | URI |
|--------|-----|
|PATCH| /mr/karaoke-box/v1/persons/{personId} |

 **Path Parameter = {personId}**

Ejemplo para el PathParameter personId

| PersonId        | DocumentNumber     |
|-----------------|--------------------|
|464564651000     | 46456465           |

Donde:
**PersonId = DocumentNumber**

### Precondiciones para el consumo de esta versión de la API
Para poder consumir el API, el canal deberá registrar su codigo en Mr.

### Headers Requeridos
| Header    | Ejemplo |
|-----------|---------|
| Request-ID | 550e8400-e29b-41d4-a716-446655440000 |
| request-date | 2017-06-01T17:15:20.509-0400 |
| app-code | KB |
| app-name | KBOX |
| caller-name | karaoke-box |

### Query Parameters válidos en esta versión de la API

No Aplica.

### Usos válidos de Query Parameters

No Aplica.

### Variantes válidas del Payload (Cuerpo del mensaje)

A continuacion un ejemplo del payload de la funcionalidad:

### Caso 1. Actualiza todos los datos de una persona natural.

```
{														
	"natural":{
		"referenceCode": "000000000",
		"fatherLastName": "VASQUEZ",
		"motherLastName": "SOTELO",                                                   	
		"names": "PETER",                                                           	
		"birthDate": "1990-03-31"
	}
}
```

### Lista de Valores usadas en esta versión de la API

A continuación se listan aquellos campos que tienen mas de un valor posible.

| Atributo |
|----------|
| request.personId |
| request.natural.ReferenceCode |
| request.natural.fatherLastName |
| request.natural.motherLastName |
| request.natural.names |
| request.natural.birthDate |

### Códigos de error usados en esta versión de la API
| Codigo | Descripcion |
|--------|-------------|
| MR0000 | El servicio se ejecutó de manera satisfactoria                           |
| MR9999  | Ocurrió un error inesperado. Por favor contactarse con Soporte Técnico  |

