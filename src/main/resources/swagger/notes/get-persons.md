### Acerca de la funcionalidad expuesta

La funcionalidad retornará la lista de clientes<br/>
<br/>
### URI de acceso a la API
| Método | URI |
|--------|-------------|
|GET| /mr/karaoke-box/v1/persons |

### Precondiciones para el consumo de esta versión de la API

Todo aplicativo que requiera consumir la API, deberá validar la existencia de su canal

### Data de Prueba

Ninguno.

### Headers Requeridos
| Header    |Ejemplo|
|-----------|-------|
|request-date|2018-01-10T17:15:20.509-0400|
|Request-ID|550e8400-e29b-41d4-a716-446655440000|
|app-code|KB|
|caller-name|mr-channel-kbox|

### Path Parameters válidos en esta version de la API

No aplica.

### Query Parameters válidos en esta version de la API

No aplica.

### Usos válidos de Query Parameters

No aplica.

### Variantes válidas del Payload (Cuerpo del mesaje)

No aplica.

### Lista de Valores usadas en esta versión de la API

No aplica.

### Código de errores del Servicio en esta versión de la API
| Codigo | Descripcion |
|--------|-------------|
| MR0000 | El servicio se ejecutó de manera satisfactoria                           |
| MR9999  | Ocurrió un error inesperado. Por favor contactarse con Soporte Técnico  |