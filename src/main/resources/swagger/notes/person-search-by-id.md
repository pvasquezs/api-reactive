### Acerca de la API

La funcionalidad permite obtener un cliente en especifico.
La búsqueda de la persona se realiza mediante el numero de identidad.

### Path Parameter = {personId}

Ejemplos para el PathParameter personId

| PersonId | DocumentNumber |
|----------|--------------|
| 72795511 | 72795511 |

Donde:
PersonId = DocumentNumber

### URI de acceso a la API
| Método | URI |
|--------|-----|
| GET | /mr/karaoke-box/v1/persons/{personId} |

### Precondiciones para el consumo de esta versión de la API
Para poder consumir el API, el canal deberá registrar su codigo en Mr.

### Headers Requeridos
| Header | Ejemplo |
|--------|---------|
| Request-ID | 550e8400-e29b-41d4-a716-446655440000 |
| request-date | 2017-06-01T17:15:20.509-0400 |
| app-code | KB |
| app-name | KBOX |
| caller-name | karaoke-box |

### Variantes válidas del PayLoad (Cuerpo del mensaje)
No Aplica.

### Códigos de errores usados en esta versión de la API
| Código | Descripción |
|--------|-------------|
| MR0000 | El servicio se ejecutó de manera satisfactoria                           |
| MR9999  | Ocurrió un error inesperado. Por favor contactarse con Soporte Técnico  |

### Ejemplos de respuesta

| Respuesta a consulta de persona natural |
|-----------------------------------------|
```json
{
  "personId": "72795511",
  "names": "PETER KOE"
}

```